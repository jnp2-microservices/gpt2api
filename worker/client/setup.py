from setuptools import setup, find_packages

setup(
    name='workerfrontendclient',
    version='0.1',
    install_requires=[
        'httpclient@git+https://gitlab.com/jnp2-microservices/gpt2api.git@httpclient-0.1#egg=httpclient&subdirectory=httpclient',
    ],
    packages=find_packages(),
)
