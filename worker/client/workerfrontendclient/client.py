from typing import Dict

from httpclient import HTTPClient


class Client(HTTPClient):
    def new_order(self, sample: str, num_paragraphs: int) -> Dict:
        """
        Creates a new order to generate `num_paragraphs` of text. The order
        can be retrieved using `view_order`.

        Return value:
        ```
        {"id": "some-id"}
        ```
        """
        return self.post(
            "/order",
            {"sample": sample, "num_paragraphs": num_paragraphs}
        )

    def view_order(self, order_id: str) -> Dict:
        """
        Retrieves a text generation order by its id. If the order is completed,
        the following is returned:
        ```
        {"status": "SUCCESS", "result": ["text_1", "text_2", ...]}
        ```
        The result field contains a list of generated texts. The list contains
        as many items as it was requested in `new_order`.

        If the order is not yet completed, this is returned:
        ```
        {"status": STATUS}
        ```
        Where `STATUS` is one of the celery states listed here:
        https://docs.celeryproject.org/en/stable/reference/celery.states.html
        """
        return self.get(f"/order/{order_id}")
