import os
import pathlib


DB_HOST = os.getenv("DB_HOST") # for example "localhost:5432"
DB_USER = os.getenv("DB_USER")
DB_PASS = os.getenv("DB_PASS")
RABBITMQ_HOST = os.getenv("RABBITMQ_HOST") # for example "localhost:5672"
RABBITMQ_USER = os.getenv("RABBITMQ_USER")
MODEL_DIR = str(pathlib.Path.home() / "gpt2-models")
MODEL_NAME = "124M"
