import os

import gpt_2_simple as gpt2

from worker.config import MODEL_DIR, MODEL_NAME


if __name__ == "__main__":
    if not os.path.isdir(os.path.join(MODEL_DIR, MODEL_NAME)):
        print(f"Downloading {MODEL_NAME} model...")
        gpt2.download_gpt2(model_dir=MODEL_DIR, model_name=MODEL_NAME)
