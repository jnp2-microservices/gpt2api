from celery import Celery

from worker.config import (
    DB_USER, DB_PASS, DB_HOST, RABBITMQ_HOST, RABBITMQ_USER, MODEL_DIR,
    MODEL_NAME
)


app = Celery(
    "orders",
    broker=f"amqp://{RABBITMQ_USER}@{RABBITMQ_HOST}//",
    backend=f"db+postgresql://{DB_USER}:{DB_PASS}@{DB_HOST}/orders"
)

@app.task(name="generate_text")
def generate_text(sample: str, n_samples: int):
    import tensorflow as tf
    import gpt_2_simple as gpt2

    if n_samples < 1 or 10 < n_samples:
        raise ValueError

    tf.reset_default_graph()
    sess = gpt2.start_tf_sess()
    gpt2.load_gpt2(sess, model_dir=MODEL_DIR, model_name=MODEL_NAME)
    return gpt2.generate(
        sess,
        prefix=sample,
        model_dir=MODEL_DIR,
        model_name=MODEL_NAME,
        nsamples=n_samples,
        return_as_list=True,
        length=256
    )
