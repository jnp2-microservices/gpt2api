from flask import Flask, request

from worker import tasks


app = Flask(__name__)

@app.route("/order", methods=["POST"])
def new_order():
    data = request.get_json(silent=True)
    if data is None:
        return {"error": "not a json"}, 400
    if not ("sample" in data and "num_paragraphs" in data):
        return {"error": "invalid json fields"}, 400
    try:
        n_samples = int(data["num_paragraphs"])
    except ValueError:
        return {"error": "num_paragraphs must be an integer"}, 400

    if n_samples < 1:
        return {"error": "invalid num_paragraphs"}, 400

    task = tasks.generate_text.delay(
        sample=str(data["sample"]),
        n_samples=n_samples
    )
    return {"id": task.id}

@app.route("/order/<order_id>", methods=["GET"])
def view_order(order_id: str):
    r = tasks.app.AsyncResult(order_id)
    if r.status == "SUCCESS":
        return {"status": r.status, "result": r.get()}
    else:
        return {"status": r.status}
