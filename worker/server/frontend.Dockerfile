# get dependencies
FROM python:3.6-buster as builder

RUN mkdir -p /app/worker
COPY requirements/common.txt /app
RUN pip install -r /app/common.txt
COPY requirements/frontend.txt /app
RUN pip install -r /app/frontend.txt

COPY setup.py /app
RUN pip install --no-cache-dir /app
COPY worker /app/worker
COPY frontend /app/frontend

# prepare the server image
FROM python:3.6-slim-buster as server

ENV SITEPACKAGES /usr/local/lib/python3.6/site-packages
ENV BIN /usr/local/bin

RUN rm -rf $SITEPACKAGES

COPY --from=builder $SITEPACKAGES $SITEPACKAGES
COPY --from=builder $BIN $BIN
COPY --from=builder /app /app

RUN pip install -e /app

CMD gunicorn -w 1 -k gevent --bind 0.0.0.0:5000 --access-logfile '-' frontend.server:app
