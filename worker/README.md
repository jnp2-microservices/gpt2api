# GPT2-WORKER and GPT2-WORKER-FRONTEND

This folder contains the source code for the GPT2-WORKER and
GPT2-WORKER-FRONTEND microservices. See
[this file](client/workerfrontendclient/client.py) for options of interacting
with them.

## Server

If you want to set these microservices up in a local environment, simply `cd`
into `server` and run `docker-compose up`. Make sure you have both `docker` and
`docker-compose` installed on your machine.

The following ports will be bound on your localhost:

- `5000`: GPT2-WORKER-FRONTEND
- `5432`: Postgres
- `5672`: RabbitMQ


## Client

If you want to use the worker frontend client in a service of your own, add
`git+https://gitlab.com/jnp2-microservices/gpt2api.git@master#subdirectory=worker/client`
to your requirements file.

You can then use it like this:

```
from workerfrontendclient import Client

client = Client("localhost", 5000)
client.new_order(...)
```
