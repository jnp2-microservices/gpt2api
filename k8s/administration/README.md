# Google Kubernetes Engine setup

Create a static ip:

```bash
gcloud compute addresses create web-static-ip --region europe-west4
```

You will need the generated ip in the next step.

Set up the NGINX ingress controller:

```bash
export STATIC_IP="change me" # e.g. 172.15.2.14
kubectl create namespace nginx-ingress
helm install nginx-ingress stable/nginx-ingress --namespace nginx-ingress --set controller.service.loadBalancerIP="$STATIC_IP"
```

Set up cert-manager:

```bash
kubectl apply --validate=false -f https://github.com/jetstack/cert-manager/releases/download/v0.15.1/cert-manager-legacy.yaml
kubectl apply -f ./cert-manager
```
