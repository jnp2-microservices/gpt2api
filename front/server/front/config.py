import os

from authclient import Client as AuthClient
from workerfrontendclient import Client as WorkerClient


DB_HOST = os.getenv("DB_HOST")
DB_USER = os.getenv("DB_USER")
DB_PASS = os.getenv("DB_PASS")
DB_DATABASE = os.getenv("DB_DATABASE")
WORKERFRONTEND_HOST = os.getenv("WORKERFRONTEND_HOST")
WORKERFRONTEND_PORT = os.getenv("WORKERFRONTEND_PORT")
AUTH_HOST = os.getenv("AUTH_HOST")
AUTH_PORT = os.getenv("AUTH_PORT")

auth_client = AuthClient(AUTH_HOST, AUTH_PORT)
worker_client = WorkerClient(WORKERFRONTEND_HOST, WORKERFRONTEND_PORT)
