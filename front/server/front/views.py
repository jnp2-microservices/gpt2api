from datetime import datetime

from flask import request
from httpclient import HTTPClientException

from front.app import db, app
from front.models import Order
from front.utils import authorize_user
from front.config import worker_client


@app.route("/order", methods=["POST"])
def new_order():
    client_id = authorize_user(request)
    if client_id is None:
        return {"error": "invalid key"}, 400

    data = request.get_json(silent=True)
    if data is None:
        return {"error": "not a json"}, 400

    if not ("sample" in data and "num_paragraphs" in data):
        return {"error": "invalid json fields"}, 400

    try:
        n_samples = int(data["num_paragraphs"])
    except ValueError:
        return {"error": "num_paragraphs must be an integer"}, 400

    if n_samples < 1:
        return {"error": "invalid num_paragraphs"}, 400

    try:
        response = worker_client.new_order(data["sample"], n_samples)
    except HTTPClientException:
        return {"error": "order could not be submitted"}, 500

    new_order = Order(client_id, response["id"], n_samples, datetime.now())
    db.session.add(new_order)
    db.session.commit()

    return {"id": new_order.order_id}

@app.route("/order", methods=["GET"])
def get_order():
    client_id = authorize_user(request)
    if client_id is None:
        return {"error": "invalid key"}, 400

    order_id = request.args.get("id")
    if order_id is None:
        return {"error": "no order id supplied"}, 400

    order = Order.query.filter_by(
        client_id=client_id, order_id=order_id).first()
    if order is None:
        return {"error": "unknown order id"}, 400

    try:
        order_details = worker_client.view_order(order_id)
    except HTTPClientException:
        return {"error": "internal server error"}, 500

    return order_details
