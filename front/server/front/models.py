import datetime

import sqlalchemy as sa

from front.app import db


class Order(db.Model):
    __tablename__ = "orders"

    id = sa.Column(sa.Integer, primary_key=True)
    client_id = sa.Column(sa.Integer(), nullable=False)
    order_id = sa.Column(sa.String(), nullable=False)
    no_paragraphs = sa.Column(sa.Integer(), nullable=False)
    date = sa.Column(sa.DateTime(), nullable=False)

    def __init__(self,
        client_id: int,
        order_id: str,
        no_paragraphs: int,
        date: datetime.datetime
    ):
        self.client_id = client_id
        self.order_id = order_id
        self.no_paragraphs = no_paragraphs
        self.date = date

    def __repr__(self):
        return (
            f"<User(client_id='{self.client_id}, 'order_id='{self.order_id}', "
            f"no_paragraphs='{self.no_paragraphs}', date='{self.date}')>"
        )
