from typing import Optional

from front.config import auth_client


def authorize_user(request) -> Optional[int]:
    api_key = request.headers.get("X-API-KEY")
    if api_key is None:
        return None
    return auth_client.get_id(api_key)    
