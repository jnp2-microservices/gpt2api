from typing import Dict

from httpclient import HTTPClient

class Client(HTTPClient):
    def new_order(self, sample: str, num_paragraphs: int) -> Dict:
        """
        Creates a new order to generate `num_paragraphs` of text. The order
        can be retrieved using `get_order`.

        Return value:
        ```
        {"id": "some-id"}
        ```
        If the authorization key is incorrect an error is returned:
        """
        return self.post(
            "/order",
            {"sample": sample, "num_paragraphs": num_paragraphs}
        )

    def get_order(self, id: str) -> Dict:
        """
        Retrieves a text generation order by its id. If the order is completed,
        the following is returned:
        ```
        {"status": "SUCCESS", "result": ["text_1", "text_2", ...]}
        ```
        If the authorization key is incorrent the following is returned:
        ```
        {"status": "INVALID KEY"}
        ```
        The result field contains a list of generated texts. The list contains
        as many items as it was requested in `new_order`.

        If the order is not yet completed, this is returned:
        ```
        {"status": STATUS}
        ```
        Where `STATUS` is one of the celery states listed here:
        https://docs.celeryproject.org/en/stable/reference/celery.states.html
        """
        return self.get(f"/order", {"id": id})