from setuptools import setup, find_packages

setup(
    name='httpclient',
    version='0.1',
    install_requires=[
        'requests==2.23.0',
    ],
    packages=find_packages()
)
