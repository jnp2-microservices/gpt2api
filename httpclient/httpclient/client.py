from typing import List, Dict, Union

import requests


class HTTPClientException(Exception):
    pass

class HTTPClient():
    def __init__(self, host: str, port: int, https: bool=False):
        if host[-1] == "/":
            host = host[:-1]
        protocol = "https" if https else "http"
    
        self.host = host
        self.port = port
        self.url = f"{protocol}://{host}:{port}"

    def get(self, path: str, data: Union[Dict, List, None]=None):
        """
        Sends a GET request to the API with the given data. Returns
        `response.json()` if the status code is `200`, otherwise raises a
        `ClientException`.
        """
        r = requests.get(url=self.url + path, params=data)
        if not (200 <= r.status_code < 300):
            raise HTTPClientException(
                f"GET {path} failed with status code {r.status_code}\n{r.text}"
            )
        return r.json()

    def post(self, path: str, data: Union[Dict, List, None]=None):
        """
        Sends a POST request to the API with the given data. Returns
        `response.json()` if the status code is `200`, otherwise raises a
        `ClientException`.
        """
        r = requests.post(url=self.url + path, json=data)
        if not (200 <= r.status_code < 300):
            raise HTTPClientException(
                f"POST {path} failed with status code {r.status_code}\n{r.text}"
            )
        return r.json()
