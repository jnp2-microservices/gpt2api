from flask import jsonify, request, make_response

from auth.app import db, app
from auth.models import User
from auth.utils import random_key


@app.route("/register", methods=["POST"])
def register():
    new_key = random_key()

    new_user = User(new_key)

    if new_user is None:
        return make_response(jsonify({"error": "cannot make new user"}), 500) 

    db.session.add(new_user)
    db.session.commit()

    return {"key": new_user.key}


@app.route("/id", methods=["GET"])
def give_id():
    user_key = request.args.get("key")
    user = User.query.filter_by(key=user_key).first()

    if user is None:
        return {"status": "DOES-NOT-EXIST"}

    return {"id": user.id, "status": "OK"}
