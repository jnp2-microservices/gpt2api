import os


DB_HOST = os.getenv("DB_HOST") # for example "localhost:5432"
DB_USER = os.getenv("DB_USER")
DB_PASS = os.getenv("DB_PASS")
DB_DATABASE = os.getenv("DB_DATABASE")
