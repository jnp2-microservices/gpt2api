from flask import Flask
from flask_sqlalchemy import SQLAlchemy


def init_app():
    from . import config

    app = Flask(__name__)
    print("App initialized")

    app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False
    app.config['SQLALCHEMY_DATABASE_URI'] = (
        f"postgresql://{config.DB_USER}:{config.DB_PASS}"
        f"@{config.DB_HOST}/{config.DB_DATABASE}"
    )

    db = SQLAlchemy(app)

    return app, db

app, db = init_app()

def init_db_tables():
    import auth.models
    db.create_all()

init_db_tables()

def init_views():
    import auth.views

init_views()
