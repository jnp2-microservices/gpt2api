import sqlalchemy as sa

from auth.app import db


class User(db.Model):
    __tablename__ = "auth"

    id = sa.Column(sa.Integer(), primary_key=True)
    key = sa.Column(sa.String(), nullable=False, unique=True)

    def __init__(self, key):
        self.key = key

    def __repr__(self):
        return f"<User(id='{self.id}', key='{self.key}')>"
