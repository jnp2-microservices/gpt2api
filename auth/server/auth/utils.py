import random


CHARS = "abcdefghijklmnopqrstuvwxyz1234567890"

def random_key() -> str:
    return "".join([random.choice(CHARS) for _ in range(20)])
