from typing import Dict, Optional

from httpclient import HTTPClient


class Client(HTTPClient):
    def register(self) -> Dict:
        """
        Register a new user.

        Return value:
        ```
        {"key": "user_id"}
        ```
        """
        return self.post("/register")

    def get_id(self, key: str) -> Optional[int]:
        """
        If the api key maps to a user, their internal id is returned. Otherwise,
        `None` is returned.
        """
        r = self.get(f"/id", {"key": key})
        return int(r["id"]) if "id" in r else None
