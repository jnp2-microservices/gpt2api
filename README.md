# GPT-2 API

In 2019 Open AI published GPT-2: a deep learning model that generates text which looks as if a human wrote it. This project aims to provide a simple to use web API that enables usage of GPT-2 without setting up a complex programming environment.

The API is temporarily hosted at https://jnp.hugodutka.com/.

# How do I use this?

We expose the following endpoints:

### POST /auth/register

Register a new user. The response is this user's API key.

Example request:

```bash
curl -X POST https://jnp.hugodutka.com/auth/register
```

Example response:

```json
{
    "key": "y9u4rhq12jq2c7xnvucx"
}
```


### POST /order

Submit an order for `num_paragraphs` paragraphs of texts based on `sample`.
`num_paragraphs` is an integer in the range [1, 10]. `sample` is a string that GPT-2 will use as an inspiration.

This endpoint requires authorization. The `X-API-KEY` header should be set to the API key received
from `POST /auth/register`.

Example request:

```bash
curl -H "X-API-KEY: y9u4rhq12jq2c7xnvucx" -H "Content-Type: application/json" -X POST "https://jnp.hugodutka.com/order" -d '{"num_paragraphs": 2, "sample": "It was a beautiful day."}'
```

Example response:

```json
{
    "id": "616704b9-939c-44ff-ab27-dc2cf13432d6"
}
```

### GET /order

Fetch the result of an order. If the order is completed the generated text is retrieved. Otherwise the order's current status is returned.

This endpoint requires authorization. The `X-API-KEY` header should be set to the API key received
from `POST /auth/register`.

Example request:

```bash
curl -H "X-API-KEY: y9u4rhq12jq2c7xnvucx" "https://jnp.hugodutka.com/order?id=616704b9-939c-44ff-ab27-dc2cf13432d6"
```

Example response for a completed order:

```json
{
    "result": [
        "It was a beautiful day and I enjoyed it. (...)",
        "It was a beautiful day. The music was playing (...)",
    ],
    "status": "SUCCESS"
}
```

Example response for a pending order:

```json    
{
    "status": "PENDING"
}
```
